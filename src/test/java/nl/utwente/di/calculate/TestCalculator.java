package nl.utwente.di.calculate;

import nl.utwente.di.bookQuote.Calculator;
import org.junit.jupiter.api.Assertions;
import org. junit . jupiter .api.Test;


public class TestCalculator {
    @Test
    public void testBook1() throws Exception{
        Calculator quoter = new Calculator();
        double price = quoter.getFahrenheit("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");
    }
}
