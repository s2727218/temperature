package nl.utwente.di.bookQuote;

import java.text.DecimalFormat;

public class Calculator {

    private final DecimalFormat df = new DecimalFormat("0.00");

    public double getFahrenheit(String celsius) {
        double fahrenheit = Double.parseDouble(celsius);
        String fahr;
        fahr = df.format((fahrenheit * (9.0/5)) + 32.0);
        return Double.parseDouble(fahr);
    }
}
